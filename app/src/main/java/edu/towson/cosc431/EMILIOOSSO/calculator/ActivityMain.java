package edu.towson.cosc431.EMILIOOSSO.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityMain extends AppCompatActivity {

    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button buttonDec;
    Button buttonClear;
    Button buttonEqual;
    Button sub;
    Button add;
    Button div;
    Button mult;
    EditText display;

    double num1;
    double num2;

    boolean addNums;
    boolean subNums;
    boolean multNums;
    boolean divNums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        buttonDec = (Button) findViewById(R.id.buttonDec);
        buttonClear = (Button) findViewById(R.id.buttonClear);
        sub = (Button) findViewById(R.id.buttonSub);
        add = (Button) findViewById(R.id.buttonAdd);
        div = (Button) findViewById(R.id.buttonDiv);
        mult = (Button) findViewById(R.id.buttonMult);
        buttonEqual = (Button) findViewById(R.id.buttonEqual);
        display = (EditText) findViewById(R.id.display);

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "0");
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "1");
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "2");
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "3");
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "4");
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "5");
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "6");
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "7");
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "8");
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + "9");
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (display != null) {
                    num1 = Double.parseDouble(display.getText() + "");
                    addNums = true;
                    display.setText("");
                }
                else {
                    display.setText("");
                }
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (display != null) {
                    num1 = Double.parseDouble(display.getText() + "");
                    subNums = true;
                    display.setText("");
                }
                else {
                    display.setText("");
                }
            }
        });

        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (display != null) {
                    num1 = Double.parseDouble(display.getText() + "");
                    divNums = true;
                    display.setText("");
                }
                else {
                    display.setText("");
                }
            }
        });

        mult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (display != null) {
                    num1 = Double.parseDouble(display.getText() + "");
                    multNums = true;
                    display.setText("");
                }
                else {
                    display.setText("");
                }
            }
        });

        buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num2 = Double.parseDouble(display.getText() + "");
                double total;
                String s;

                if (addNums == true) {
                    total = num1 + num2;
                    s = Double.toString(total);
                    display.setText(s);
                }
                else if (subNums == true) {
                    total = num1 - num2;
                    s = Double.toString(total);
                    display.setText(s);
                }
                else if (multNums == true) {
                    total = num1 * num2;
                    s = Double.toString(total);
                    display.setText(s);
                }
                else if (divNums == true) {
                    if (num2 == 0) {
                        display.setText("Error");
                    }
                    else {
                        total = num1 / num2;
                        s = Double.toString(total);
                        display.setText(s);
                    }
                }
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText("");
                if (display.getText() == null) {
                    num1 = Double.parseDouble("");
                }
            }
        });

        buttonDec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + ".");
            }
        });

    }
}
